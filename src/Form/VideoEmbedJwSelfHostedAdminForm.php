<?php

namespace Drupal\video_embed_jw_self_hosted\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;

/**
 * Administrative form for global JW Player settings.
 *
 * @package Drupal\video_embed_jw_self_hosted\Form
 */
class VideoEmbedJwSelfHostedAdminForm extends ConfigFormBase {

  /**
   * The Jw Player (self hosted) Service.
   *
   * @var \Drupal\video_embed_jw_self_hosted\Service\JwSelfHostedService
   */
  protected $jw;

  /**
   * The drupal config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $settings;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->jw = \Drupal::service('jw_self_hosted.service');
    $this->settings = \Drupal::config('video_embed_jw_self_hosted.settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'video_embed_jw_self_hosted.settings',
    ];
  }

  /**
   * Returns just the form id.
   *
   * @return string
   *   The form id
   */
  public function getFormId() {
    return 'video_embed_jw_self_hosted_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['account_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Account settings'),
      '#description' => $this->t('You can find your API credentials <a href=":url" target="_blank">here</a>.', [':url' => URL::fromUri('https://dashboard.jwplayer.com/#/account/apicredentials')->toString()]),
      '#collapsed' => FALSE,
    ];

    $form['account_settings']['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key'),
      '#description' => $this->t('The JW Player API key'),
      '#default_value' => $this->settings->get('key'),
    ];

    $form['account_settings']['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#description' => $this->t('The JW Player API secret'),
      '#default_value' => $this->settings->get('secret'),
    ];

    $form['player_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Player settings'),
      '#collapsed' => FALSE,
    ];

    $form['player_settings']['player'] = [
      '#type' => 'select',
      '#title' => $this->t('Player'),
      '#description' => $this->t('Select your player of choice.') . '<br/><b>' . $this->t('If disabled you need to provide at least an api key and secret.') . '</b>',
      '#options' => $this->jw->getPlayers(),
      '#disabled' => empty($this->settings->get('key')) && empty($this->settings->get('secret')),
      '#default_value' => $this->settings->get('player'),
    ];

    $form['player_settings']['template'] = [
      '#type' => 'select',
      '#title' => $this->t('Template'),
      '#description' => $this->t('Select your template of choice.') . '<br/><b>' . $this->t('If disabled you need to provide at least an api key and secret.') . '</b>',
      '#options' => $this->jw->getTemplates(),
      '#disabled' => empty($this->settings->get('key')) && empty($this->settings->get('template')),
      '#default_value' => $this->settings->get('template'),
    ];

    $form['security_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Security'),
      '#description' => $this->t('Security settings regarding JW Player (self hosted) videos.'),
    ];

    $form['security_settings']['expiry'] = [
      '#type' => 'number',
      '#title' => $this->t('Expiry'),
      '#description' => $this->t('How long should the video be accessible (in seconds)?'),
      '#min' => 60,
      '#default_value' => empty($this->settings->get('expiry')) ? $this->jw->getExpiry() : $this->settings->get('expiry'),
      '#field_suffix' => $this->t('Seconds'),
    ];

    $form['clear_cache'] = [
      '#type' => 'details',
      '#title' => $this->t('Clear cache'),
      '#description' => $this->t('In case you changed the credentials or added a new player / template at your JW Player account.'),
      '#open' => TRUE,
    ];

    $form['clear_cache']['clear'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear the JW Player (self hosted) caches'),
      '#submit' => ['::submitCacheClear'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('video_embed_jw_self_hosted.settings')
      ->set('key', $form_state->getValue('key'))
      ->set('secret', $form_state->getValue('secret'))
      ->set('player', $form_state->getValue('player'))
      ->set('template', $form_state->getValue('template'))
      ->set('expiry', $form_state->getValue('expiry'))
      ->save();
  }

  /**
   * Clears the caches.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state array.
   */
  public function submitCacheClear(array &$form, FormStateInterface $form_state) {
    Cache::invalidateTags(['jw_self_hosted']);
    drupal_set_message($this->t('Caches cleared.'));
  }

}
