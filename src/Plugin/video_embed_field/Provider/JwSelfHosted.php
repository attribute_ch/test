<?php

namespace Drupal\video_embed_jw_self_hosted\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;
use GuzzleHttp\ClientInterface;

/**
 * A custom JW Player provider plugin.
 *
 * @VideoEmbedProvider(
 *  id = "jw_self_hosted",
 *  title = @Translation("JW Player (Self hosted)")
 * )
 */
class JwSelfHosted extends ProviderPluginBase {

  /**
   * The default width in pixels of the remote thumbnail to download.
   *
   * @var int
   */
  const THUMBNAIL_WIDTH = 720;

  /**
   * The JW Player (self hosted) Service.
   *
   * @var \Drupal\video_embed_jw_self_hosted\Service\JwSelfHostedService
   */
  protected $jw;

  /**
   * Create a plugin with the given input.
   *
   * Calls the parent construct and adds the $jw variable as the jw service.
   *
   * @param array $configuration
   *   The configuration of the plugin.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An HTTP client.
   *
   * @throws \Exception
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client);
    $this->jw = \Drupal::Service('jw_self_hosted.service');
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay, $player = '', $template = '') {
    $url_file = $this->jw->buildSignedUrl('https://content.jwplatform.com/', sprintf('videos/%s-%s', $this->getMediaId(), $template), '.mp4');
    $url_image = $this->getRemoteThumbnailUrl();

    // Creates the div wit hall information.
    $attachment = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('Loading video...'),
      '#attributes' => [
        'id' => 'jw-self-hosted-' . $this->getMediaId(),
        'data-file' => $url_file,
        'data-image' => $url_image,
      ],
    ];
    // Adds the script tag to the html head.
    $attachment['#attached']['html_head'][] = [
      [
        '#type' => 'html_tag',
        '#tag' => 'script',
        '#attributes' => [
          'src' => $this->jw->buildSignedUrl('https://content.jwplatform.com/', 'libraries/' . $player, '.js'),
        ],
      ],
      'video_embed_jw_self_hosted.playerscript',
    ];
    // Adds the module javascript.
    $attachment['#attached']['library'][] = 'video_embed_jw_self_hosted/video_embed_jw_self_hosted';

    return [$attachment];
  }

  /**
   * A wrapper just to return the video id.
   *
   * @return null|string
   *   The media id
   */
  public function getMediaId() {
    if ($mediaId = $this->getVideoId()) {
      return $mediaId;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * Parses input in the format of:
   * string matching:
   * - 8 characters long
   * - lowercase characters
   * - UPPERCASE characters
   * - integers
   * e.g. "eT60TKU1"
   */
  public static function getIdFromInput($input) {
    if (preg_match('/^(?<id>[a-zA-Z0-9]{8})$/', $input, $matches)) {
      return !empty($matches['id']) ? $matches['id'] : FALSE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return $this->jw->buildThumbnailUrl($this->getMediaId(), $this->getThumbnailWidth());
  }

  /**
   * Determine the remote thumbnail width to download.
   *
   * See https://developer.jwplayer.com/jw-platform/docs/delivery-api-reference/#!/poster_images/get_thumbs_media_id_thumb_width_jpg.
   *
   * @return int
   *   The width in pixels.
   */
  public function getThumbnailWidth() {
    return $this::THUMBNAIL_WIDTH;
  }

}
