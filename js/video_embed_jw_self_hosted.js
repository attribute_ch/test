(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.video_embed_jw_self_hosted = {
    attach: function (context, settings) {

      $('[id^="jw-self-hosted-"]', context).once('video_embed_jw_self_hosted').each(function(){
        var $video_container = $(this);
        settings.la_jw = settings.la_jw || {};
        settings.la_jw.players = settings.la_jw.players || [];
        settings.la_jw.players.push(
            jwplayer($video_container.attr("id")).setup({
              'file':  $video_container.data('file'),
              'image':  $video_container.data('image')
            })
        );
      });
    }
  };

})(jQuery, Drupal, drupalSettings);